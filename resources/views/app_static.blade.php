@extends('layouts.master')

@section('cssfile')
{{-- Styles --}}
@stop

@section('jsfile')
{{-- Scripts --}}
@stop

@section('content')
<div class="relative flex items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0">
    <div class="max-w-6xl mx-auto sm:px-6 lg:px-8">
        <div class="grid grid-cols-4 gap-4">
            <div class="col-start-2 col-span-2">
                {{-- PROFILE --}}
                <div class="flex justify-center pt-8 sm:pt-0 mb-4">
                    <div class="flex items-start p-4">
                        <div class="flex items-center justify-center w-16 h-16 mx-2 overflow-hidden rounded-full">
                            <img src="https://images.unsplash.com/photo-1548544149-4835e62ee5b3?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=100&q=80">
                        </div>
                  
                        <div class="ml-4">
                            <h2 class="text-2xl font-bold">
                                Display Name
                            </h2>
                            <p class="mt-2 text-sm text-gray-500">Last opened 4 days ago</p>
                        </div>
                    </div>
                </div>

                {{-- TOP ARTIST --}}
                <div class="flex pt-0 sm:pt-0 mt-6">
                    <h3 class="text-xl font-bold mb-2">
                        Top Artist
                    </h3>
                </div>

                <div class="mt-0 flex flex-wrap">
                    <span class="mr-2 my-1 px-2 py-1 bg-gray-800 text-gray-200 text-sm font-semibold inline">
                        Artist Name
                    </span>
                </div>

                {{-- TOP TRACKS --}}
                <div class="flex pt-0 sm:pt-0 mt-6">
                    <h3 class="text-xl font-bold mb-2">
                        Top Artist
                    </h3>
                </div>

                <div class="mt-0 flex flex-wrap">
                    <span class="mr-2 my-1 px-2 py-1 bg-gray-800 text-gray-200 text-sm font-semibold inline">
                        Artist Name
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('scripts')
{{-- Inline Scripts --}}
@stop
