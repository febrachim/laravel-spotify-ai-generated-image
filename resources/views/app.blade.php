@extends('layouts.master')

@section('cssfile')
{{-- Styles --}}
@stop

@section('jsfile')
{{-- Scripts --}}
@stop

@section('content')
<div class="relative flex justify-center items-top min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0">
    <div class="max-w-6xl mx-auto sm:px-6 lg:px-8">
        <div class="grid grid-cols-4 gap-4">
            <div class="col-start-2 col-span-2">
                {{-- PROFILE --}}
                <div class="flex justify-center mt-8 mb-4 bg-white rounded-lg drop-shadow-md">
                    <div class="flex items-start p-4">
                        <div class="flex items-center w-16 h-16 mx-2 overflow-hidden rounded-full">
                            <img src="{{ $profile->images[0]->url }}">
                        </div>
                  
                        <div class="ml-2">
                            <h2 class="text-2xl font-bold">
                                {{ $profile->display_name }}
                            </h2>
                            <p class="mt-0 text-sm text-gray-500">{{ $profile->email }}</p>
                        </div>
                    </div>
                </div>
                
                <div class="mb-4 bg-white p-4 rounded-lg drop-shadow-md">
                    {{-- TOP ARTIST --}}
                    <div class="flex justify-center">
                        <h3 class="text-xl font-bold mb-2">
                            Top Artist
                        </h3>
                    </div>

                    <div class="mt-0 flex flex-wrap justify-center">
                        @foreach ($topArtists->items as $item)
                        <span class="mr-2 my-1 px-2 py-1 bg-gray-800 text-gray-200 text-sm font-semibold inline">
                            {{ $item->name }}
                        </span>
                        @endforeach
                    </div>

                    {{-- TOP TRACKS --}}
                    <div class="flex mt-6 justify-center">
                        <h3 class="text-xl font-bold mb-2">
                            Top Artist
                        </h3>
                    </div>

                    <div class="mt-0 flex flex-wrap justify-center">
                        @foreach ($topTracks->items as $item)
                        <span class="mr-2 my-1 px-2 py-1 bg-gray-800 text-gray-200 text-sm font-semibold inline">
                            {{ $item->name }}
                        </span>
                        @endforeach
                    </div>
                </div>
                
                {{-- AI Generated Image --}}
                <div class="mb-4 bg-white p-4 rounded-lg drop-shadow-md">
                    <div class="flex justify-center">
                        <h3 class="text-xl font-bold mb-2">
                            AI Generated Image
                        </h3>
                    </div>
                    <div class="flex justify-center">
                        <div class="flex items-center w-auto mx-2">
                            <img src="https://images.unsplash.com/photo-1644424235870-bd212a5e67a5?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1887&q=80">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('scripts')
{{-- Inline Scripts --}}
@stop
