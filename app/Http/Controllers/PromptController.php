<?php

namespace App\Http\Controllers;

use Spotify;
use Laravel\Socialite\Facades\Socialite;

class PromptController extends Controller
{
    /**
     * Display a listing of the resource.
     * @param int $id
     * @return Response
     */
    public function index()
    {
        $a = array('a' => 'waw');
        $b = array('a' => 'wooow');

        $albums = Spotify::searchTracks('Closed on Sunday')->get();

        return view('prompt')->with(
            array(
                'a' => $a,
                'b' => $b,
                'albums' => $albums
            )
        );
    }
}
