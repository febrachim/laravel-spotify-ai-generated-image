<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;
use Auth;
use Laravel\Socialite\Facades\Socialite;
use Spotify;
use Exception;
use SpotifyWebAPI\SpotifyWebAPI;
use SpotifyWebAPI\Session;

class SpotifyController extends Controller
{
    public function index(Request $request) {
        $request->session()->forget('accessToken');
        return view('index');
    }

    public function signInwithSpotify() {
        $session = new Session(
            env('SPOTIFY_CLIENT_ID'),
            env('SPOTIFY_CLIENT_SECRET'),
            env('SPOTIFY_REDIRECT_URI')
        );

        $state = $session->generateState();
        $options = [
            'scope' => [
                'playlist-read-private',
                'user-read-private',
                'user-read-email',
                'user-top-read',
            ],
            'state' => $state,
        ];

        return redirect($session->getAuthorizeUrl($options));
        // return Socialite::driver('spotify')->redirect();
    }
    
    public function callbackToSpotify() {
        $session = new Session(
            env('SPOTIFY_CLIENT_ID'),
            env('SPOTIFY_CLIENT_SECRET'),
            env('SPOTIFY_REDIRECT_URI')
        );

        $state = $_GET['state'];
        // Request a access token using the code from Spotify
        $session->requestAccessToken($_GET['code']);

        $accessToken = $session->getAccessToken();
        $refreshToken = $session->getRefreshToken();

        // Store the access and refresh tokens somewhere. In a session for example
        return redirect()->route('spotifyApp', [
            'accessToken' => $accessToken,
            'refreshToken' => $refreshToken
        ]);
    }
    
    public function spotifyApp() {
        $accessToken = $_GET['accessToken'];
        $refreshToken = $_GET['refreshToken'];

        $api = new SpotifyWebAPI();

        // Fetch the saved access token from somewhere. A session for example.
        $api->setAccessToken($accessToken);

        $profile = $api->me();
        $topArtist = $api->getMyTop('artists', ['limit' => 5]);
        $topTracks = $api->getMyTop('tracks', ['limit' => 5]);

        return view('app')->with(
            array(
                'profile' => $profile,
                'topArtists' => $topArtist,
                'topTracks' => $topTracks,
            )
        );
    }
    
    public function static() {
        return view('app_static');
    }
}
