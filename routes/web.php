<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SpotifyController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [SpotifyController::class, 'index']);

Route::group(['prefix'=>'prompt', 'as'=>'prompt.'], function(){
	Route::controller(App\Http\Controllers\PromptController::class)->group(function () {
        Route::get('/', 'index')->name('index');
        Route::get('/redirect', 'redirect')->name('redirect');
    });
});

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified'
])->group(function () {
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');
});

Route::get('auth/spotify', [SpotifyController::class, 'signInwithSpotify']);
Route::get('callback', [SpotifyController::class, 'callbackToSpotify']);
Route::get('app', [SpotifyController::class, 'spotifyApp'])->name('spotifyApp');
Route::get('static', [SpotifyController::class, 'static'])->name('static');